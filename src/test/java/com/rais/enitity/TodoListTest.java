package com.rais.enitity;

import com.rais.enums.TaskProgressEnum;
import org.junit.Test;

import static org.junit.Assert.*;

public class TodoListTest {

    @Test
    public void testGetTodolist(){
        String expected = "1. Ngoding - Belajar [DONE]\n2. Belajar CPP - Belajar [NOT_DONE]\n3. Dota 2 - Hiburan [DONE]";
        TodoList todoList = new TodoList();
        assertEquals(expected, todoList.getTodoList());
    }

    @Test
    public void testGetTodolistById(){
        String expected = "1. Ngoding - Belajar [DONE]";
        TodoList todoList = new TodoList();
        assertEquals(expected, todoList.getTaskById(1));
    }

    @Test
    public void testUpdateTaskExpectedTaskIsUpdated(){
        String expected = "2. Belajar CPP - Belajar [DONE]";
        TodoList todoList = new TodoList();
        todoList.markProgressDoneById(2);
        assertEquals(expected, todoList.getTaskById(2));
    }

    @Test
    public void testUpdateTaskExpectedAlreadyUpdated(){
        String expected = "Task Already Done";
        TodoList todoList = new TodoList();
        assertEquals(expected, todoList.markProgressDoneById(1));
    }

    @Test
    public void testUpdateTaskExpectedIndexOutOfBounds(){
        String expected = "Todolist Not Found";
        TodoList todoList = new TodoList();
        assertEquals(expected, todoList.markProgressDoneById(0));
    }

    @Test
    public void testDeleteTaskById(){
        int idWantToDelete = 2;
        TodoList todoList = new TodoList();
        todoList.deleteTaskById(idWantToDelete);
        assertEquals(todoList.getTaskById(idWantToDelete),"Task Not Found");
    }

    @Test
    public void testAddNewTaskToTodolist(){
        TodoList todoList = new TodoList();
        todoList.addNewTaskToTodolist("Fortnite","Hiburan",TaskProgressEnum.NOT_DONE);
        String expected = "4. Fortnite - Hiburan [NOT_DONE]";
        assertEquals(expected,todoList.getTaskById(4));
    }

    @Test
    public void testGetTaskByCategory() {
        String expected = "1. Ngoding - Belajar [DONE]\n2. Belajar CPP - Belajar [NOT_DONE]";
        TodoList todoList = new TodoList();
        assertEquals(expected,todoList.getTaskbyCategory("Belajar"));
    }

    @Test
    public void testIsCategoryExpectedTrue() {
        Task task = new Task(4, "Main Gitar", "Hobby", TaskProgressEnum.NOT_DONE);
        assertTrue(task.isCategory("Hobby"));
    }

    @Test
    public void testIsCategoryExpectedFalse() {
        Task task = new Task(4, "Main Gitar", "Hobby", TaskProgressEnum.NOT_DONE);
        assertFalse(task.isCategory("Hobbs"));
    }


    @Test
    public void testGetTaskByCategoryExpectedEmpty() {
        String expected = "Category not found";;
        TodoList todoList = new TodoList();
        assertEquals(expected, todoList.getTaskbyCategory("Main Games"));
    }

}