package com.rais.enitity;

import com.rais.enums.TaskProgressEnum;
import org.junit.Test;

import static org.junit.Assert.*;

public class TaskTest {

    @Test
    public void testGetTask(){
        String expected = "1. Ngoding - Belajar [DONE]";
        Task task = new Task(1, "Ngoding", "Belajar",TaskProgressEnum.DONE);
        assertEquals(expected, task.toString());
    }


}