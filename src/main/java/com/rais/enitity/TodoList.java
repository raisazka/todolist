package com.rais.enitity;

import com.rais.enums.TaskProgressEnum;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TodoList {
    private List<Task> taskList;

    public TodoList() {
        add(new Task(1, "Ngoding", "Belajar", TaskProgressEnum.DONE));
        add(new Task(2, "Belajar CPP", "Belajar", TaskProgressEnum.NOT_DONE));
        add(new Task(3, "Dota 2", "Hiburan", TaskProgressEnum.DONE));
    }
    public void addNewTaskToTodolist(String name, String category, TaskProgressEnum progress) {
        int id = taskList.get(taskList.size() -1).getId();
        add(new Task(id + 1,name,category, progress));
    }

    public void add(Task task) {
        if (taskList == null) {
            taskList = new ArrayList<>();
        }
        taskList.add(task);
    }

    public String getTodoList() {
        return taskList.stream()
                .map(task -> task.getId() + ". " + task.getName() + " - " + task.getCategory() + " [" + task.getProgress() + "]")
                .collect(Collectors.joining("\n"));
    }

    public String getTaskById(int id) {
        for (Task task : taskList) {
            if (task.getId() == id) {
                return task.toString();
            }
        }
        return "Task Not Found";
    }

    public String markProgressDoneById(int id) {
        for (Task task : taskList) {
            if (task.getId() == id) {
                if (task.getProgress() == TaskProgressEnum.DONE) {
                    return "Task Already Done";
                }
                task.setProgress(TaskProgressEnum.DONE);
            }
        }
        return "Todolist Not Found";
    }

    public String deleteTaskById(int id) {
        for (Task task : taskList) {
            if (task.getId() == id) {
                taskList.remove(task);
                return "Task Deleted";
            }
        }
        return "Todolist Not Found";
    }

    public String getTaskbyCategory(String category) {
        try {
            StringBuilder tasks = new StringBuilder();
            for(Task task : taskList) {
                if(task.isCategory(category)) {
                    tasks.append(task.toString());
                    tasks.append("\n");
                }
            }
            tasks.setLength(tasks.length() - 1);
            return tasks.toString();
        } catch (IndexOutOfBoundsException e) {
            return "Category not found";
        }
    }

}