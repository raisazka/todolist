package com.rais.enitity;

import com.rais.enums.TaskProgressEnum;

public class Task {
    private int id;
    private String name;
    private String category;
    private TaskProgressEnum progress;

    public Task(int id, String name, String category,TaskProgressEnum progress) {
        this.id = id;
        this.name = name;
        this.category = category;
        this.progress = progress;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCategory() {
        return category;
    }

    public TaskProgressEnum getProgress() {
        return progress;
    }

    public void setProgress(TaskProgressEnum progress) {
        this.progress = progress;
    }

    @Override
    public String toString() {
        return getId() + ". " + getName() +" - " + getCategory() + " [" + getProgress() + "]";
    }

    public boolean isCategory(String category) {
       return this.category.equalsIgnoreCase(category);
    }
}