package com.rais.view;

import com.rais.enitity.TodoList;
import com.rais.enums.TaskProgressEnum;

import java.util.Scanner;

public class Menu {
    Scanner scanner;
    TodoList todoList;

    public Menu() {
        todoList = new TodoList();
        scanner = new Scanner(System.in);
    }

    public void initMenu() {
        int choose;
        do {
            initBaseMenu();
            choose = scanner.nextInt();
            scanner.nextLine();
            switch (choose){
                case 1:
                    displayTask();
                    break;
                case 2:
                    displayTodoList();
                    break;
                case 3:
                    updateTheTaskProgress();
                    break;
                case 4:
                    deleteTask();
                    break;
                case 5:
                    searchTaskByCategory();
                    break;
                case 6 :
                    addNewTask();
                    break;
            }
        }while (choose != 7);
    }

    private void addNewTask() {
        String name;
        String category;
        System.out.println("Input Task Name :");
        name = scanner.nextLine();
        System.out.println("Input Category :");
        category = scanner.nextLine();
        todoList.addNewTaskToTodolist(name,category,TaskProgressEnum.NOT_DONE);
    }

    private void initBaseMenu() {
        System.out.println("1. Get Task");
        System.out.println("2. Get Todolist");
        System.out.println("3. Update Task Progress");
        System.out.println("4. Delete Task");
        System.out.println("5. Search by category");
        System.out.println("6. Add new Task");
        System.out.println("7. Exit");
        System.out.println("Choose Menu: ");
    }

    private void displayTask() {
        int index;
        System.out.println("Choose The Task Id: ");
        index = scanner.nextInt();
        System.out.println("===============");
        System.out.println(todoList.getTaskById(index));
        System.out.println("===============");
        System.out.println();
    }

    private void displayTodoList() {
        System.out.println("===============");
        System.out.println(todoList.getTodoList());
        System.out.println("===============");
        System.out.println();
    }

    private void updateTheTaskProgress() {
        int index;
        System.out.println("Choose The Task Id: ");
        index = scanner.nextInt();
        System.out.println(todoList.markProgressDoneById(index));
        System.out.println("===============");
    }

    private void deleteTask() {
        int index;
        System.out.println("Choose The Task Id: ");
        index = scanner.nextInt();
        todoList.deleteTaskById(index);
        System.out.println("===============");
    }

    private void searchTaskByCategory() {
        System.out.println("Choose The Task Category: ");
        String category = scanner.nextLine();
        System.out.println("===============");
        System.out.println(todoList.getTaskbyCategory(category));
        System.out.println("===============");
    }


}
