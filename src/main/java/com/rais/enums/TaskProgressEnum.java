package com.rais.enums;

public enum TaskProgressEnum {
    DONE,
    NOT_DONE
}
